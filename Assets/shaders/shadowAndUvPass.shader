﻿// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'

Shader "custom/shadowPass_withCookie"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
	}
		SubShader
	{
		Pass
		{
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase
			#pragma multi_compile_lightpass
			// shadow helper functions and macros
			#include "AutoLight.cginc"

			uniform sampler2D _LightTexture0;
		 uniform float4x4 unity_WorldToLight; // transformation 

		struct v2f
		{
			float2 uv : TEXCOORD0;
			SHADOW_COORDS(1) // put shadows data into TEXCOORD1
			fixed3 diff : COLOR0;
			fixed3 ambient : COLOR1;
			float4 pos : SV_POSITION;
			float4 posLight : TEXCOORD1;
		};
		v2f vert(appdata_base v)
		{
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv = v.texcoord;
			half3 worldNormal = UnityObjectToWorldNormal(v.normal);
			half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
			o.diff = nl * _LightColor0.rgb;
			o.ambient = ShadeSH9(half4(worldNormal,1));
			

			float4x4 modelMatrix = unity_ObjectToWorld;
			o.posLight = mul(unity_WorldToLight, mul(modelMatrix, v.vertex));

			//o.posLight = _WorldSpaceLightPos0;
			// compute shadows data
			TRANSFER_SHADOW(o)
			return o;
		}

		fixed4 _Color;

		fixed4 frag(v2f i) : SV_Target
		{

		fixed4 col = _Color;
		// compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
		fixed shadow = SHADOW_ATTENUATION(i);
		// darken light's illumination with shadow, keep ambient intact
		fixed3 lighting = i.diff * shadow + i.ambient;

		float cookieAttenuation = 1.0;
		cookieAttenuation = tex2D(_LightTexture0, i.posLight.xy).a;

		col.rgb *= lighting* cookieAttenuation;
		return col;
	}
	ENDCG
}

// shadow casting support
UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}