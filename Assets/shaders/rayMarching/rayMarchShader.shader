﻿
Shader "Unlit/rayMarchShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Radius("Radius", float) = 1
		_Centre("Centre", Vector) = (0,0,0,0)

		_Color("rgbMultColor", Color) = (1,1,1,1)
		_SpecularPower("spec", float) = 1
		_Gloss("gloss", float) = 1
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "LightMode" = "ForwardBase" }
		LOD 100
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
					// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "SimplexNoise3D.hlsl"
			//#include "ClassicNoise3D.hlsl"

			float3 _Centre;
			float _Radius;
			float3 _Color;
			float _SpecularPower;
			float _Gloss;
			#define STEPS 64
			#define MIN_DISTANCE 0.0001

			float fractalNoise(float3 p, int iterations) {

				float o = 0;
				float s = .5;
				float w = 0.5;
				for (int i = 0; i < iterations; i++) {
					o += snoise(p*s)*w;
					//s *= 2;
					//w *= 0.5;
					s *= 10.0;
					w *= 0.1;
				}
				return o;
			}
			float sdf_sphere(float3 p, float3 c, float r)
			{
				return distance(p, c) - r;
			}
			float sphereDistance(float3 p)
			{
				return distance(p, _Centre) - _Radius;
			}
			float sdf_smin(float a, float b, float k = 32)
			{
				float res = exp(-k * a) + exp(-k * b);
				return -log(max(0.0001, res)) / k;
			}
			float sdf_blend(float d1, float d2, float a)
			{
				return a * d1 + (1 - a) * d2;
			}
			float map(float3 p)
			{
				float f = fractalNoise(p + float3(0, 0, 0), 2);
				return f;
				return max(f,-.001);
				//float f = snoise(p)*.1;
				//return sdf_sphere(p, _Centre, _Radius);
				return sdf_sphere(p, _Centre, _Radius)+ f*.5;
				return sdf_smin
				(
					sdf_sphere(p, _Centre, _Radius), // Left sphere
					//snoise(p*.5) *.5
					sdf_sphere(p, float3 (3, 0, 0),3),  // Right sphere
					2
				);
			}
			float3 normal(float3 p)
			{
				const float eps = .0001;
				return normalize(
					float3(	map(p + float3(eps, 0, 0)) - map(p - float3(eps, 0, 0)),
							map(p + float3(0, eps, 0)) - map(p - float3(0, eps, 0)),
							map(p + float3(0, 0, eps)) - map(p - float3(0, 0, eps))
					)
				);
			}
			fixed4 simpleLambert(fixed3 normal, fixed3 viewDirection) {
				fixed3 lightDir = _WorldSpaceLightPos0.xyz; // Light direction
				fixed3 lightCol = _LightColor0.rgb; // Light color

				fixed NdotL = max(dot(normal, lightDir), 0);
				//fixed NdotL = max(dot(normal, lightDir) + 1, 0)*.5;// overshoot light
				fixed4 c;
				c.rgb = _Color * lightCol * NdotL;
				c.a = 1;

				//// Specular
				//fixed3 h = (lightDir - viewDirection) / 2.;
				//fixed s = pow(dot(normal, h), _SpecularPower) * _Gloss;
				//c.rgb = _Color * lightCol * NdotL + s;
				//c.a = 1;

				return c;
			}
			fixed4 renderSurface(float3 p, fixed3 viewDirection, float iter = 0)
			{
				float3 n = normal(p);
				return simpleLambert(n, viewDirection) + float4(iter,0,0,0);
			}
			fixed4 raymarch(float3 position, float3 direction)
			{
				for (int i = 0; i < STEPS; i++)
				{
					// give a posision (starting at the surface of the real geometry)
					// put that position in the noise field
					// if it comes out a number bigger than MIN_DISTANCE, then shoot a ray, dist multiplied by the number, inside to get a new position.
					// try: make sure the numbers retrieved are not too high, so the rays dont take such big steps. or remap it somehow so it eases slower to the exact surface and doesnt skip wanted details
					float distance = map(position); 
					//try: instead of checking if it smaller than (it can also lay deep inside the negative field.) try to make the distance value absolute. so if its inside too much it will go back to the surface..
					if (distance < MIN_DISTANCE)
						return renderSurface(position, direction, i/ 50);

					// If the position was not inside the map or close to the surface than MIN_DISTANCE, make a new position in the direction of viewing multiplied with the distance removed from the surface.
					// The position will each time get closer and closer to the surface untill its smaller than MIN_DISTANCE, then we stop the loop.
					position += distance * direction;
				}
				return 0;
			}

			struct v2f {
				float4 pos : SV_POSITION; // Clip space
				float3 wPos : TEXCOORD1; // World position
			};
			// Vertex function
			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.wPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}


			fixed4 frag(v2f i) : SV_Target
			{
				float3 viewDirection = normalize(i.wPos - _WorldSpaceCameraPos);
				float3 worldPosition = i.wPos;
				return raymarch(worldPosition, viewDirection);
				//if (raymarchHit(worldPosition, viewDirection))
				//	return fixed4(1,0,0,1); // Red if hit the ball
				//else
				//	return fixed4(1,1,1,1); // White otherwise
			}
				ENDCG
			}
	}
}
