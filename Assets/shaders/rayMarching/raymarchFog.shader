﻿
Shader "Unlit/rayMarchShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Radius("Radius", float) = 1
		_Density("Density", float) = .05
		_Centre("Centre", Vector) = (0,0,0,0)
		_noiseSpeed("NoiseSpeed", Vector) = (0,0,0)

		_Color("rgbMultColor", Color) = (1,1,1,1)
		_SpecularPower("spec", float) = 1
		_Gloss("gloss", float) = 1
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "LightMode" = "ForwardBase" }
		LOD 100
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
					// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "SimplexNoise3D.hlsl"
			//#include "ClassicNoise3D.hlsl"

			float3 _Centre;
			float3 _noiseCenter;
			float3 _noiseSpeed;
			float _Radius;
			float _Density;
			float3 _Color;
			float _SpecularPower;
			float _Gloss;
			#define STEPS 16
			#define MIN_DISTANCE 0.0001

			sampler2D _CameraDepthTexture;

			float fractalNoise(float3 p, int iterations) {

				float o = 0;
				float s = 3;
				float w = 1;
				for (int i = 0; i < iterations; i++) {
					o += snoise(p*s)*w;
					//s *= 2;
					//w *= 0.5;
					s *= 10.0;
					w *= 0.1;
				}
				return o;
			}
			float sdf_sphere(float3 p, float3 c, float r)
			{
				return distance(p, c) - r;
			}
			float sdf_smin(float a, float b, float k = 32)
			{
				float res = exp(-k * a) + exp(-k * b);
				return -log(max(0.0001, res)) / k;
			}
			float sdf_blend(float d1, float d2, float a)
			{
				return a * d1 + (1 - a) * d2;
			}
			float map(float3 p){
				//return sdf_sphere(p, _Centre, _Radius);

				float f = fractalNoise(p + _noiseCenter, 1);
				//return f;
				//return max(f,-.001);
				//float f = snoise(p)*.1;
				//
				return sdf_sphere(p, _Centre, _Radius)+ f*.5;
				return sdf_smin
				(
					sdf_sphere(p, _Centre, _Radius), // Left sphere
					//snoise(p*.5) *.5
					sdf_sphere(p, float3 (3, 0, 0),3),  // Right sphere
					2
				);
			}
			float3 normal(float3 p)
			{
				const float eps = .0001;
				return normalize(
					float3(	map(p + float3(eps, 0, 0)) - map(p - float3(eps, 0, 0)),
							map(p + float3(0, eps, 0)) - map(p - float3(0, eps, 0)),
							map(p + float3(0, 0, eps)) - map(p - float3(0, 0, eps))
					)
				);
			}
			fixed4 simpleLambert(fixed3 normal, fixed3 viewDirection) {
				fixed3 lightDir = _WorldSpaceLightPos0.xyz; // Light direction
				fixed3 lightCol = _LightColor0.rgb; // Light color

				fixed NdotL = max(dot(normal, lightDir), 0);
				//fixed NdotL = max(dot(normal, lightDir) + 1, 0)*.5;// overshoot light
				fixed4 c;
				c.rgb = _Color * lightCol * NdotL;
				c.a = 1;

				//// Specular
				//fixed3 h = (lightDir - viewDirection) / 2.;
				//fixed s = pow(dot(normal, h), _SpecularPower) * _Gloss;
				//c.rgb = _Color * lightCol * NdotL + s;
				//c.a = 1;

				return c;
			}
			fixed4 renderSurface(float3 p, fixed3 viewDirection, float iter = 0)
			{
				float3 n = normal(p);
				return simpleLambert(n, viewDirection) + float4(iter,0,0,0);
			}
			fixed4 raymarch(float3 position, float3 direction, float3 camPos, float3 worldspaceGeo)
			{
				float geometryCamDist = distance(camPos, worldspaceGeo);
				float distSurfaceToGeometry = geometryCamDist - distance(camPos, position);
				float3 stepVec = (direction* distSurfaceToGeometry) / STEPS;

				// return this for non mapped fog
				//return fixed4(distSurfaceToGeometry, 0, 0, 1);

				float opacity = 0;
				for (int i = 0; i < STEPS; i++)
				{
					// give a posision (starting at the surface of the real geometry)
					// put that position in the noise field
					// if it comes out a number bigger than MIN_DISTANCE, then shoot a ray, dist multiplied by the number, inside to get a new position.
					// try: make sure the numbers retrieved are not too high, so the rays dont take such big steps. or remap it somehow so it eases slower to the exact surface and doesnt skip wanted details
					float dist = map(position); 
					opacity += clamp(dist*-_Density* distSurfaceToGeometry,0,1);
					//try: instead of checking if it smaller than (it can also lay deep inside the negative field.) try to make the distance value absolute. so if its inside too much it will go back to the surface..
					//if (dist < MIN_DISTANCE)
					//	return fixed4(1, 0, 0, 1- i / STEPS);
					//	//return renderSurface(position, direction, i/ 50);

					// If the position was not inside the map or close to the surface than MIN_DISTANCE, make a new position in the direction of viewing multiplied with the distance removed from the surface.
					// The position will each time get closer and closer to the surface untill its smaller than MIN_DISTANCE, then we stop the loop.
					position += stepVec;

					// when the ray reached existing geometry, show the gathered opacity
					if (distance(camPos, position) > geometryCamDist)
						return fixed4(1,0,0, opacity);
				}
				return fixed4(1, 0, 0, opacity);
			}

			struct v2f {
				float4 pos : SV_POSITION; // Clip space
				float3 wPos :	  TEXCOORD1; // World position
				float4 screenPosition : TEXCOORD2;
				float3 worldDirection : TEXCOORD3;
			};
			// Vertex function
			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.wPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.screenPosition = o.pos;
				o.worldDirection = mul(unity_ObjectToWorld, v.vertex).xyz - _WorldSpaceCameraPos;
				return o;
			}


			fixed4 frag(v2f i) : SV_Target
			{
				_noiseCenter = _noiseSpeed*_Time;

				float2 uv = i.screenPosition.xy / i.screenPosition.w;
				//uv.y = uv.y* -0.5 + 0.5f;
				uv.y = uv.y* 0.5 + 0.5f;
				uv.x = uv.x* 0.5 + 0.5f;
				float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv));

				// TODO: depth doesnt seem to be the real distance from the camera.
				// looking top down to a plane I should see a gradient since points in the middle of the screen are in world space closer than points towards the edge
				// Compute projective scaling factor...
				float perspectiveDivide = 1.0f / i.screenPosition.w;
				float3 direction = i.worldDirection * perspectiveDivide;
				float3 worldspaceGeo = direction * depth + _WorldSpaceCameraPos;
				//return fixed4(worldspace, 1);

				float3 worldPosition = i.wPos;
				return raymarch(worldPosition, normalize(i.worldDirection), _WorldSpaceCameraPos, worldspaceGeo);
				//if (raymarchHit(worldPosition, viewDirection))
				//	return fixed4(1,0,0,1); // Red if hit the ball
				//else
				//	return fixed4(1,1,1,1); // White otherwise
			}
				ENDCG
			}
	}
}
