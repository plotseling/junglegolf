﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/volumeShadow" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_Depth("Depth Fade", Float) = 1.0
		_Fix("Depth Distance", Float) = -0.09
	}
		SubShader{
		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
		LOD 200

		ZWrite Off

	Pass
	{
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		#include "UnityCG.cginc"

		sampler2D _CameraDepthTexture;

	half _Depth;
	half _Fix;

	fixed4 _Color;

	struct v2f
	{
		float4 pos : SV_POSITION;
		float4 uv : TEXCOORD0;
		float3 color : COLOR;
	};

	v2f vert(appdata_base v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = ComputeScreenPos(o.pos);
		//o.worldNorm = UnityObjectToWorldNormal (v.normal);
		//o.viewDir = WorldSpaceViewDir (v.vertex);


		float3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
		float dotProduct = dot(v.normal, viewDir);
		float rimWidth = .9;
		o.color = smoothstep(0, 1, dotProduct);
		//o.color = viewDir;

		return o;
	}

	half4 frag(v2f i) : SV_Target
	{
		float2 uv = i.uv.xy / i.uv.w;

		//float rim = saturate (dot (normalize (i.viewDir), i.worldNorm));

		half lin = LinearEyeDepth(tex2D(_CameraDepthTexture, uv).r);
		half dist = i.uv.w - _Fix;
		half depth = lin - dist;

		depth = clamp(depth, 0, 1)*clamp(i.uv.w, 0, 1)*i.color.r*i.color.r;
		depth *= depth;
		//depth = lin;
		//// H is the viewport position at this pixel in the range -1 to 1.
		//float4 H = float4((i.uv.x) * 2 - 1, (i.uv.y) * 2 - 1, depth, 1);
		//float4 D = mul((camera.projectionMatrix * camera.worldToCameraMatrix).inverse;// (passed from your script), H);
		//return D / D.w;


		//return half4 (depth, depth, depth, 1);
		return half4 (0, 0, 0,  depth);
		
		//return lerp(half4 (1,1,1,0), _Color, saturate(depth * _Depth));
	}
		ENDCG
	}
	}
		FallBack "Diffuse"
}