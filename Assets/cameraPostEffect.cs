﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
[ImageEffectAllowedInSceneView]
public class cameraPostEffect : MonoBehaviour
{

    public Material mat;

    public Transform pos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        var camera = GetComponent<Camera>();
        mat.SetMatrix("_ViewProjectInverse", (camera.projectionMatrix * camera.worldToCameraMatrix).inverse);
        mat.SetVector("_volumePosition", new Vector4(pos.position.x, pos.position.y, pos.position.z, 0));

        Graphics.Blit(source, destination, mat);
    }
}
