﻿using UnityEngine;
using System.Collections;

public class ChangeBallLayer : MonoBehaviour {

    public int LayerOnEnter; // BallInHole
    public int LayerOnExit;  // BallOnTable


    void OnTriggerEnter(Collider other)
    {
        print("entering");
        Physics.IgnoreLayerCollision(0, other.gameObject.layer, true);

    }

    void OnTriggerExit(Collider other)
    {
        print("leaving");
        Physics.IgnoreLayerCollision(0, other.gameObject.layer, false);

    }
}