﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slideOnTrigger : MonoBehaviour
{

    bool move = false;
    float moveAmt = 0;
    float moveSpeed = 0;
    Vector3 startPos;
    MeshRenderer mr;

    public Transform targetTransform;
    public Transform collideWith;
    public float height = 1;
    public bool hardReset = false;
    public bool linearLerp = true;
    public bool hideOnRest = false;

    public Transform nextSpawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        mr = GetComponent<MeshRenderer>();

        if (hideOnRest)
            mr.enabled = false;

        if(targetTransform == null)
        {
            targetTransform = new GameObject().transform;
            targetTransform.position = transform.position;
            targetTransform.Translate(new Vector3(0, height, 0), Space.Self);
        }
    }

    // Update is called once per frame
    void Update()
    {

        float dist = (targetTransform.position - transform.position).magnitude;
        if (move)
        {
            //print("sqmfd:lksmlfdksd");
            moveAmt += .01f;
            moveSpeed = .01f;
            if (hideOnRest)
                mr.enabled = true;

            // set nextSpawnPoint
            if (nextSpawnPoint != null)
                collideWith.GetComponent<resetPosition>().setPos(nextSpawnPoint);
        }
        else
        {
            moveAmt -= .01f;
            if (dist < .0001f)
            {// wait untill position is reached
                moveSpeed = 0f;
                if (hardReset)
                    transform.position = startPos;
                if (hideOnRest)
                    mr.enabled = false;
            }
        }

        if(linearLerp)
            transform.position = Vector3.Lerp(startPos, targetTransform.position, moveAmt);
        else
            transform.position = Vector3.Lerp(transform.position, targetTransform.position, moveSpeed);

        moveAmt = Mathf.Clamp01(moveAmt);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == collideWith)
        {
            move = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform == collideWith)
            move = false;
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, nextSpawnPoint.position);
    }
}
