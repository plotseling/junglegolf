﻿namespace VRTK.UnityEventHelper
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class impactSound : MonoBehaviour
    {

        VRTK_InteractHaptics ih;

        public AudioSource sound;
        public Transform interactObject;

        public bool trigger = false;
        public bool ifGrabbed = false;
        bool playOnce = true;

        // Start is called before the first frame update
        void Start()
        {
            //ih.in
        }

        // Update is called once per frame
        void Update()
        {
            if (ifGrabbed)
            {
                if (playOnce)
                {
                    if (GetComponent<FixedJoint>() != null)
                    {
                        sound.Play();
                        playOnce = false;
                    }
                }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {

            if (!trigger && !ifGrabbed)
            {
                if (collision.transform.name == interactObject.name)
                {

                    sound.volume = collision.relativeVelocity.magnitude*3;

                    sound.Play();

                    //GetComponent<HapticPulseEventHandler>().EventHandler temp = MyEvent;
                    //if (temp != null)
                    //{
                    //    temp();
                    //}
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (trigger && !ifGrabbed)
            {
                if (other.transform.name == interactObject.name)
                {
                    sound.Play();
                }
            }
        }
    }
}