﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trail : MonoBehaviour
{


    public GameObject strand;
    public float density = 10f;
    public float spread = .1f;
    Vector3 prevPos;
    float totalDistance = 0;
    float lastPlacedDist = 0;

    // Start is called before the first frame update
    void Start()
    {
        prevPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = (transform.position - prevPos).magnitude;
        totalDistance += dist;

        float speed = GetComponent<Rigidbody>().velocity.magnitude;
        if (speed > .1)
            cloneBetweenPoints(strand, prevPos, transform.position, speed);

        prevPos = transform.position;
    }

    void cloneBetweenPoints(GameObject g, Vector3 a, Vector3 b, float speed)
    {
        float distanceDifference = totalDistance - lastPlacedDist;
        int count = (int)((distanceDifference* density)/speed);

        count = Mathf.Clamp(count, 0, 5);

        for (int i = 0; i < count; i++)
        {
            GameObject n = Instantiate(g, Vector3.Lerp(a,b,(float)i/count), g.transform.rotation);
            n.transform.position += new Vector3(Random.value * spread - spread*.5f, 0, Random.value * spread - spread * .5f);
            n.transform.position = new Vector3(n.transform.position.x, 0, n.transform.position.z);

            n.transform.localRotation = Quaternion.Euler(-0, Random.value*360,0);

            n.SetActive(true);
        }

        if(count > 0)
            lastPlacedDist = totalDistance;
    }

}
