﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitCounter : MonoBehaviour
{

    int hitCount = 0;

    public TextMesh textM;
    public Transform collideWith;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform == collideWith)
        {
            hitCount++;
            textM.text = hitCount.ToString();
        }
    }
}
