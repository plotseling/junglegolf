﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetPosition : MonoBehaviour
{

    public float minimumYpos = -10;
    public Transform spawnPoint;
    Vector3 origPos;
    Quaternion origRot;

    

    // Start is called before the first frame update
    void Start()
    {
        if (spawnPoint != null)
            setPos(spawnPoint);
        else
            setPos(transform);
    }

    public void setPos(Transform t)
    {
        origPos = t.position;
        origRot = t.rotation;
        reset();
    }

    void reset()
    {
        transform.position = origPos;
        transform.rotation = origRot;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < minimumYpos)
        {
            reset();
        }
    }
}
