﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attract : MonoBehaviour
{

    public Transform target;

    public float radius = 100;
    public float strength = .1f;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = target.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 dir = transform.position - target.position;

        if(dir.magnitude < radius)
            rb.AddForce(dir.normalized* strength);

    }
}
