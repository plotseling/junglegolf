﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class connecTrackingStick : MonoBehaviour
{

    public Transform stick;
    public Transform target;
    bool connected;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!connected)
        {
            if((int)GetComponent<SteamVR_TrackedObject>().index == 3)
            {
                if (stick.gameObject.GetComponent<FixedJoint>() == null)
                {

                    print(transform.name);


                    //align
                    stick.position = target.position;
                    stick.rotation = target.rotation;

                    //connect
                    FixedJoint fj = stick.gameObject.AddComponent<FixedJoint>();
                    Rigidbody rb = target.gameObject.AddComponent<Rigidbody>();
                    rb.isKinematic = true;
                    fj.connectedBody = rb;
                    connected = true;
                }
            }
        }
    }
}
