﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ignoreColliders : MonoBehaviour
{

    public Transform floor;

    void Start()
    {
    }

    void Update()
    {
        if(GetComponent<FixedJoint>() != null)
        {
            Physics.IgnoreCollision(floor.GetComponent<Collider>(), GetComponent<Collider>(), true);

            //ignore everything and stick
            Physics.IgnoreLayerCollision(0, 10, true);
        }
        else
        {
            Physics.IgnoreCollision(floor.GetComponent<Collider>(), GetComponent<Collider>(), false);
            Physics.IgnoreLayerCollision(0, 10, false);
        }
    }


}
