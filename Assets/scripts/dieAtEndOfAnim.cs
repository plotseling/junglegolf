﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dieAtEndOfAnim : MonoBehaviour
{
    float age = 0;
    public float ageLimit = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        age += Time.deltaTime;
        if (age > ageLimit)
            Destroy(gameObject);
    }
}
