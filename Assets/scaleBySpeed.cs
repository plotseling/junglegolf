﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scaleBySpeed : MonoBehaviour
{

    Vector3 prevPos;
    Vector3 difference;

    public float direction = 10;

    // Start is called before the first frame update
    void Start()
    {
        prevPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        difference = transform.position - prevPos;

        float relative_xPos = transform.InverseTransformVector(difference).x  * direction;

        if (relative_xPos > 0)
            transform.localScale = new Vector3(relative_xPos*5 + 1, relative_xPos + 1, relative_xPos + 1);
        else
            transform.localScale = new Vector3(1,1,1);


        prevPos = transform.position;

    }
}
