﻿using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
public class colorAtlasEditor : EditorWindow
{

    Color[] matColor = { Color.white, Color.white, Color.white };

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/My Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        colorAtlasEditor window = (colorAtlasEditor)EditorWindow.GetWindow(typeof(colorAtlasEditor));
        window.Show();
    }

    void OnGUI()
    {
        int i = 0;
        foreach (Color c in matColor)
        {
            matColor[i] = EditorGUILayout.ColorField("subobject "+i, c);
            i++;
        }

        if (GUILayout.Button("set!"))
            ChangeColors();

        if (GUILayout.Button("sat more!"))
            saturate(1.05f,0);
        if (GUILayout.Button("sat less!"))
            saturate(.95f,0);
        if (GUILayout.Button("bright more!"))
            saturate(1,.01f);
        if (GUILayout.Button("bright less!"))
            saturate(1, -.01f);

        if (GUILayout.Button("RANDOM ALL!"))
            randomAll(1, -.01f);
    }

    void OnSelectionChange()
    {

        if (Selection.activeGameObject)
            foreach (GameObject t in Selection.gameObjects)
            {
                Renderer rend = t.GetComponent<Renderer>();

                int i = 0;
                if (rend != null) {
                    //for (int i = 0, j = meshInst.subMeshCount; i < j; i += 1)
                    //{
                    //    HashSet<int> subMeshTris = new HashSet<int>(meshInst.GetTriangles(i));

                    //    //for (int k = Mathf.Min(subMeshTris), l = Mathf.Max(subMeshTris); k <= l, k += 1)
                    //    //{
                    //    //    //partUvs[k] = new Vector2(partUVs[k].x + shiftX, partUVs[k].y - shiftY);
                    //    //}
                    //}

                    foreach (Material m in rend.sharedMaterials)
                    {
                        //Texture2D tex = m.GetTexture("_MainTex") as Texture2D;
                        //matColor[i] = tex.GetPixel(0, 0);
                        matColor[i] = m.color;
                        i++;
                    }
                }
            }
        Repaint();
    }

    void Update()
    {

    }

    private void randomAll(float mult, float add)
    {

        foreach (MeshRenderer rend in UnityEngine.Object.FindObjectsOfType<MeshRenderer>())
        {
            if (rend != null)
            {
                foreach (Material m in rend.sharedMaterials)
                {
                    m.color = matColor[0] + new Color(Random.value*.5f, Random.value * .5f, Random.value * .5f);
                }
            }
        }
    }

    private void saturate(float mult, float add)
    {
        if (Selection.activeGameObject)
            foreach (MeshRenderer rend in Selection.gameObjects[0].GetComponentsInChildren<MeshRenderer>())
            {
                if (rend != null)
                {
                    foreach (Material m in rend.sharedMaterials)
                    {
                        m.color = m.color*mult;
                        m.color = new Color(m.color.r + add, m.color.g + add, m.color.b + add);
                    }
                }
            }
    }

    private void ChangeColors()
    {
        if (Selection.activeGameObject)
            foreach (GameObject t in Selection.gameObjects)
            {
                Renderer rend = t.GetComponent<Renderer>();

                if (rend != null)
                    rend.sharedMaterial.color = matColor[0];
            }
    }


    private void getUvs()
    {
        if (Selection.activeGameObject)
        {
            GameObject t = Selection.gameObjects[0];
            Mesh mesh = t.GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = mesh.vertices;
            Vector2[] uvs = new Vector2[vertices.Length];

            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
            }
            mesh.uv = uvs;
        }
    }
}
#endif