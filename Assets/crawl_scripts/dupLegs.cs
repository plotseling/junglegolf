﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dupLegs : MonoBehaviour {
    public int count = 2;
    public Transform legRoot;

    // Use this for initialization
    void Start()
    {

        for (int i = 0; i < count; i++)
        {
            GameObject dup = Instantiate(legRoot.gameObject);
            dup.transform.Translate(new Vector3(i*.2f,0,0));
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
