﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tube : MonoBehaviour {

    Vector3[] vertices;

    // Use this for initialization
    void Start () {
        MeshFilter filter = gameObject.AddComponent<MeshFilter>();
        Mesh mesh = filter.mesh;
        mesh.Clear();

        float height = 1f;
        float bottomRadius = 3f;
        float topRadius = .05f;
        int nbSides = 12;
        int segments = 1;

        #region Vertices
        vertices = new Vector3[nbSides * (segments+1)];
        //int vert = 0;
        float _2pi = Mathf.PI * 2f;
        int v = 0;
        for (int seg = 0; seg <= segments; seg++)
        {
            for (int vert = 0; vert < nbSides; vert++)
            {
                float rad = (float)v / nbSides * _2pi;
                vertices[nbSides* seg + vert] = new Vector3(Mathf.Cos(rad) * bottomRadius,  height+seg,  Mathf.Sin(rad) * bottomRadius);
                v++;
            }
        }
        print(vertices.Length);
        print(v);
        #endregion


        //#region Normals
        //Vector3[] normales = new Vector3[vertices.Length];
        //vert = 0;
        //v = 0;
        //while (vert <= vertices.Length - 4)
        //{
        //    float rad = (float)v / nbSides * _2pi;
        //    float cos = Mathf.Cos(rad);
        //    float sin = Mathf.Sin(rad);

        //    normales[vert] = new Vector3(cos, 0f, sin);
        //    normales[vert + 1] = normales[vert];

        //    vert += 2;
        //    v++;
        //}
        //#endregion


        //#region UVs
        //Vector2[] uvs = new Vector2[vertices.Length];
        //int u = 0;
        //int u_sides = 0;
        //while (u <= uvs.Length - 4)
        //{
        //    float t = (float)u_sides / nbSides;
        //    uvs[u] = new Vector3(t, 1f);
        //    uvs[u + 1] = new Vector3(t, 0f);
        //    u += 2;
        //    u_sides++;
        //}
        //uvs[u] = new Vector2(1f, 1f);
        //uvs[u + 1] = new Vector2(1f, 0f);
        //#endregion



        #region Triangles
        int triangleCount = vertices.Length ;
        int[] triangles = new int[triangleCount * 3];
        //int[] triangles = new int[nbSides * 2 * (segments + 1)];
        //tri++;
        print(triangles.Length);
        for (int tri = 0; tri < nbSides - 1; tri++)
        {
            print(tri);
            triangles[tri * 6] = tri + 0;
            triangles[tri * 6 + 1] = tri + 1;
            triangles[tri * 6 + 2] = tri + nbSides;

            triangles[tri * 6 + 3] = tri + 1;
            triangles[tri * 6 + 4] = tri + nbSides+1;
            triangles[tri * 6 + 5] = tri + nbSides;

            //triangles[tri + 3] = tri + 2;
            //triangles[tri + 4] = tri + nbSides;
            //triangles[tri + 5] = tri + 1;
            //tri++;
            //i += 3;
        }
        #endregion


        mesh.vertices = vertices;
        //mesh.normals = normales;
        //mesh.uv = uvs;
        mesh.triangles = triangles;

        mesh.RecalculateBounds();
    }

    // Update is called once per frame
    void Update()
    {

        for (int vert = 0; vert < vertices.Length-1; vert++)
        {
            Debug.DrawLine(vertices[vert], vertices[vert + 1]);
        }

    }
}
