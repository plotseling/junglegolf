﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class limitLifetime : MonoBehaviour {

	int age = 0;
	public int maxAge = 100;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(age > maxAge){
			Destroy (gameObject);
		}
		age++;
	}
}
