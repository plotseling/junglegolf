﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class bezierShape : MonoBehaviour
{

    public Transform[] pointList;
    Vector3[] points;

    int quantity = 10;
    Bezier[] bezierLineList;

    LineRenderer Line;

    public Color lineColor = Color.cyan;

    // Use this for initialization
    void Start()
    {
        bezierLineList = new Bezier[1];

        points = new Vector3[bezierLineList.Length * (quantity + 1)];

        Line = GetComponent<LineRenderer>();

        if (Line != null)
            Line.positionCount = points.Length;
    }

    void Update()
    {
    }

    public void updateLines()
	{
		//points = new Vector3[bezierLineList.Length * (quantity + 1) - 1 ];
		int insertAt = 0;
		for (int i = 0; i < bezierLineList.Length; i++) {

			Bezier bezier = new Bezier ();

			bezier.pointsQuantity = quantity;
			bezier.firstPoint = pointList [i * 3].position;
			bezier.handlerFirstPoint = pointList [i * 3 + 1].position;
			bezier.handlerSecondPoint = pointList [i * 3 + 2].position;
			bezier.secondPoint = pointList [i * 3 + 3].position;
            
            bezierLineList [i] = bezier;

			Vector3[] pnts = bezier.calculatePoints ();
			pnts.CopyTo (points, insertAt);
			insertAt += quantity;
        }


        for (int i = 0; i < points.Length - 1; i++)
        {
			Debug.DrawLine(points[i], points[i + 1], lineColor);
        }

        if(Line != null)
            Line.SetPositions(points);

    }
}