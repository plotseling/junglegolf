﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leaveStripes : MonoBehaviour {


	public GameObject stripeObj;
	Transform parent;

	public float spacing;
	float totalTraveledDistance;
	float traveledDistance;

	Vector3 prevPosLocal;
	Vector3 prevStripe;

	// Use this for initialization
	void Start () {
		parent = transform.parent;
	}
	
	// Update is called once per frame
	void Update () {
		traveledDistance += (transform.localPosition-prevPosLocal).magnitude;
//		totalTraveledDistance += (transform.position-prevPosLocal).magnitude;

		if (traveledDistance > spacing)
			dropStripe ();

		prevPosLocal = transform.localPosition;
	}

	void dropStripe(){
		traveledDistance = 0;
		Vector3 walkDirection = (transform.localPosition - prevStripe).normalized;
		Transform newStripe = Instantiate (stripeObj,  parent).transform;
		newStripe.localPosition = transform.localPosition;
		newStripe.localRotation = Quaternion.LookRotation (walkDirection, transform.up);
		prevStripe = transform.localPosition;
	}
}
