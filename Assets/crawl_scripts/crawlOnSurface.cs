using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class crawlOnSurface : MonoBehaviour {
    public GameObject target;
    public Transform chaseObj;
    Vector3 a, b, c;

    public float radius = .002f;

    Vector3 awayFromPointLerped;

    Transform currentParent;
    float shootFromDistance = 3f;

    Vector3 pt;
    Vector3[] ptList;
    Transform[] pt_2_resultList;

    Vector3 resultPtPrevious;
    Vector3 normal;
    Vector3[] closestVerts;
    Transform parent;

    public float crawlSpeed = 1;
    public float deviationStrength = 1;
    float deviation = 0;

    Vector3[] verts;
    Mesh mesh;
    int[] tris;

    public Transform[] tailObjects;
    public Vector3[] tailOffsets;

    void Start() {
        resultPtPrevious = new Vector3(0, 0, 0);
        awayFromPointLerped = new Vector3(0, 0, 0);
        closestVerts = new Vector3[3];
        parent = transform.parent;

        MeshFilter mf = target.GetComponent<MeshFilter>();
        mesh = mf.mesh;
        verts = mesh.vertices;

        currentParent = transform.parent;

        Transform offsetFrom = transform;
        tailOffsets = new Vector3[tailObjects.Length];
        for (int i = 0; i < tailObjects.Length; i++)
        {
            tailOffsets[i] = offsetFrom.position - tailObjects[i].position;
            offsetFrom = tailObjects[i];
        }

        //        for (int i = 0; i < 10; i++)
        //        {
        //            burnedList.Add(new Vector3(0, 0, 0));
        //            // make array of last 10 places
        //            // take average position of it.
        //            // compare current position to this average position (burnedSpot) and store 10/distance to the burnedMultiplier
        //            // use distance to multiply away from the burnedSpot.
        //        }

        //		ptList = new Vector3[nt];
        //		pt_2_resultList = new Transform[];
        //		for (int i = 0; i < dupCount; i++) {
        //			pt_2_resultList[i] =  instanciate(gameObject);
        //			pt_2_resultList[i].localScale= new Vector3(0.06f, 0.06f, 0.06f);
        //			pt_2_resultList[i].SetParent(parent);
        //		}
    }

    void Update() {

        NearestPointOnMesh(pt, transform, resultPtPrevious);

        resultPtPrevious = transform.localPosition;
        
        deviation = (.5f - Mathf.PerlinNoise(transform.localPosition.x * .3f + Time.frameCount * .01f, transform.localPosition.y * .3f)) * deviationStrength;

        float facing = 0;
        if (chaseObj != null)
        {
            facing = Vector3.Angle(transform.up, chaseObj.position - target.transform.position);

            Vector3 awayFromPoint = Vector3.RotateTowards(transform.up, (target.transform.position - chaseObj.position).normalized, Mathf.PI / 2f, 0.0f);
            Debug.DrawRay(transform.position, awayFromPoint, Color.red);
            awayFromPointLerped = Vector3.Lerp(awayFromPointLerped, awayFromPoint, .005f);
        }


        //checkOtherObjects();


        if (facing > 30) {
            pt = transform.localPosition
                + (parent.InverseTransformDirection(awayFromPointLerped.normalized) * crawlSpeed
                + parent.InverseTransformDirection(transform.TransformDirection(new Vector3(1, 0, 0))) * deviation) * Time.deltaTime;
        } else {
            pt = transform.localPosition
                + (parent.InverseTransformDirection(transform.forward) * crawlSpeed
                + parent.InverseTransformDirection(transform.TransformDirection(new Vector3(1, 0, 0))) * deviation) * Time.deltaTime;
        }

        // away from other bugs
        dupOnSurface d = target.GetComponent<dupOnSurface>();
        if (d != null)
        {
            pt += parent.InverseTransformDirection(awayFromPoints(target.GetComponent<dupOnSurface>().bugs)) * crawlSpeed;
        }


        for (int i = 0; i < tailObjects.Length; i++){
            tailObjects[i].position = NearestPointOnMesh(pt + tailOffsets[i]);
        }

    }

    Vector3 awayFromPoints(Transform[] bugs) {
        Vector3 awayVec = new Vector3(0, 0, 0);
        int othersThatAreClose = 0;
        for (int i = 0; i < bugs.Length; i++) {
            if (bugs[i].gameObject == gameObject)
                continue;
            float dist = (bugs[i].position - transform.position).magnitude;
            //			print (bugs [i].position);
            if (dist < radius) {
                othersThatAreClose++;
                //				if (othersThatAreClose == 0)
                if (dist == 0)
                    dist = .0001f; // so we dont divide by 0
                awayVec += (bugs[i].position - transform.position).normalized * .1f * (1 - radius / dist);
                //				if (othersThatAreClose > 1)
                //					awayVec = Vector3.RotateTowards(awayVec, (transform.position - points[i])	, Mathf.PI / 2f, 0.0f);
                //				Debug.DrawRay(transform.position, (bugs [i].position - transform.position).normalized * (1-radius/dist), Color.yellow);
            }
        }

        Debug.DrawRay(transform.position, awayVec, Color.magenta);
        //		print (othersThatAreClose);
        //		return new Vector3 (0,0,0);
        if (othersThatAreClose == 0)
            return awayVec;
        return awayVec / othersThatAreClose;
    }

    void checkOtherObjects(){
        
        RaycastHit hit;
        Debug.DrawRay(transform.position + transform.TransformDirection(new Vector3(0, 1 * shootFromDistance, 1 * shootFromDistance)),
            transform.TransformDirection(new Vector3(0, -1 * shootFromDistance, 0)), Color.yellow);

        if (Physics.Raycast(new Ray(
            transform.position + transform.TransformDirection(new Vector3(0, 1 * shootFromDistance, 1 * shootFromDistance)),
            transform.TransformDirection(new Vector3(0, -1 * shootFromDistance, 0))), out hit))
        {
            //			print (hit.transform.name);
            //			print (hit.triangleIndex);
            if (hit.transform != currentParent && hit.transform != transform)
            {
                Debug.DrawRay(hit.point, hit.normal * 100, Color.white);
                currentParent = hit.transform;
                transform.SetParent(currentParent);
                target = currentParent.gameObject;

                MeshFilter mf = target.GetComponent<MeshFilter>();
                mesh = mf.mesh;
                verts = mesh.vertices;

                // add check if hit obj is not a child of transform
            }
        }

    }

    //	void OnDrawGizmos() {
    ////		Gizmos.color = Color.red;
    ////		for (int i = 0; i < ptList.Length; i++) {
    ////			if (parent != null) {
    ////				Gizmos.DrawSphere (parent.TransformPoint (ptList [i]), .03f);
    ////			}
    //		//		}
    //		Gizmos.color = Color.green;
    //		Gizmos.DrawRay(transform.position, target.transform.TransformPoint(normal));
    //
    //	}

    public Vector3 NearestPointOnMesh(Vector3 pt, Transform placeObj = default(Transform), Vector3 resultPtPrevious = default(Vector3)) {

        int[] tri = mesh.triangles;
        Vector3 nearestPt = Vector3.zero;
		float nearestSqDist = 100000000f;
		for (int i = 0; i < tri.Length; i+=3) {
			Vector3 a = verts[tri[i]];
			Vector3 b = verts[tri[i + 1]];
			Vector3 c = verts[tri[i + 2]];
			Vector3 possNearestPt = NearestPointOnTri(pt, a, b, c);
			float possNearestSqDist = (pt - possNearestPt).sqrMagnitude;
			if (possNearestSqDist < nearestSqDist) {
				nearestPt = possNearestPt;
				nearestSqDist = possNearestSqDist;
				closestVerts[0] = a;
				closestVerts[1] = b;
				closestVerts[2] = c;
			}
		}
        

		normal = Vector3.Cross(closestVerts[0]-closestVerts[1], closestVerts[0]-closestVerts[2]).normalized;
        if (placeObj != null) {
            // place obj
            placeObj.localPosition = nearestPt;
            // orient obj
            if (resultPtPrevious != null) {
                if (transform.localPosition - resultPtPrevious != Vector3.zero)
                {
                    Quaternion normalRot = Quaternion.LookRotation(transform.localPosition - resultPtPrevious, normal);
                    placeObj.localRotation = Quaternion.Lerp(placeObj.localRotation, normalRot, .1f);
                }
            }
        }
        
        return target.transform.TransformPoint(nearestPt);
    }

	//	Vector3 avgDirection(Vector3[] vecs,Vector3 point){
//		for (int i = 0; i < vecs.Length; i++) {
//			float dist = (point - vecs [0]).magnitude;
//		}
//	}
	public Vector3 NearestPointOnTri(Vector3 pt, Vector3 a, Vector3 b, Vector3 c) {
		Vector3 edge1 = b - a;
		Vector3 edge2 = c - a;
		Vector3 edge3 = c - b;
		float edge1Len = edge1.magnitude;
		float edge2Len = edge2.magnitude;
		float edge3Len = edge3.magnitude;
		
		Vector3 ptLineA = pt - a;
		Vector3 ptLineB = pt - b;
		Vector3 ptLineC = pt - c;
		Vector3 xAxis = edge1 / edge1Len;
		Vector3 zAxis = Vector3.Cross(edge1, edge2).normalized;
		Vector3 yAxis = Vector3.Cross(zAxis, xAxis);
		
		Vector3 edge1Cross = Vector3.Cross(edge1, ptLineA);
		Vector3 edge2Cross = Vector3.Cross(edge2, -ptLineC);
		Vector3 edge3Cross = Vector3.Cross(edge3, ptLineB);
		bool edge1On = Vector3.Dot(edge1Cross, zAxis) > 0f;
		bool edge2On = Vector3.Dot(edge2Cross, zAxis) > 0f;
		bool edge3On = Vector3.Dot(edge3Cross, zAxis) > 0f;
		
	//	If the point is inside the triangle then return its coordinate.
		if (edge1On && edge2On && edge3On) {
			float xExtent = Vector3.Dot(ptLineA, xAxis);
			float yExtent = Vector3.Dot(ptLineA, yAxis);
			return a + xAxis * xExtent + yAxis * yExtent;
		}
		
	//	Otherwise, the nearest point is somewhere along one of the edges.
		Vector3 edge1Norm = xAxis;
		Vector3 edge2Norm = edge2.normalized;
		Vector3 edge3Norm = edge3.normalized;
		
		float edge1Ext = Mathf.Clamp(Vector3.Dot(edge1Norm, ptLineA), 0f, edge1Len);
		float edge2Ext = Mathf.Clamp(Vector3.Dot(edge2Norm, ptLineA), 0f, edge2Len);
		float edge3Ext = Mathf.Clamp(Vector3.Dot(edge3Norm, ptLineB), 0f, edge3Len);

		Vector3 edge1Pt = a + edge1Ext * edge1Norm;
		Vector3 edge2Pt = a + edge2Ext * edge2Norm;
		Vector3 edge3Pt = b + edge3Ext * edge3Norm;
		
		float sqDist1 = (pt - edge1Pt).sqrMagnitude;
		float sqDist2 = (pt - edge2Pt).sqrMagnitude;
		float sqDist3 = (pt - edge3Pt).sqrMagnitude;
		
		if (sqDist1 < sqDist2) {
			if (sqDist1 < sqDist3) {
				return edge1Pt;
			} else {
				return edge3Pt;
			}
		} else if (sqDist2 < sqDist3) {
			return edge2Pt;
		} else {
			return edge3Pt;
		}
	}
}
