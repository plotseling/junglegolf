﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dupOnSurface : MonoBehaviour {

	public Transform parent;
	public Transform chaseObj;
	public int count;
	public GameObject obj;
	public bool setCrawlSurface;

	public float crawlSpeed = 50;
	public float deviationSpeed = 50;

	public Transform[] bugs;
	public Vector3[] bugPosList;

	// Use this for initialization
	void Start () {
		bugs = new Transform[count];
		bugPosList = new Vector3[count];
		for (int i = 0; i < count; i++) {
			GameObject dup = Instantiate (obj,gameObject.transform);
			dup.transform.position = new Vector3 (Random.Range(-1f,1f),Random.Range(-1f,1f),Random.Range(-1f,1f));
			dup.GetComponent<crawlOnSurface> ().target = gameObject;
			dup.GetComponent<crawlOnSurface> ().chaseObj = chaseObj;
			dup.GetComponent<crawlOnSurface> ().crawlSpeed = crawlSpeed*Random.Range(.9f,1.1f);
			dup.GetComponent<crawlOnSurface> ().deviationStrength = deviationSpeed;

			bugs [i] = dup.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < count; i++) {
			bugPosList [i] = bugs [i].position;
		}
	}
}
