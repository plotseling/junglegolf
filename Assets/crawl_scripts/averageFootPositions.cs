﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class averageFootPositions : MonoBehaviour {

	Transform[] legs;
	Transform[] feet;
	Vector3 averagePosition;
	Vector3 balanceDeviation;
	Vector3 center;
	int legCount;

    public Transform lookDirObj;
    Vector3 walkDirection;
	Vector3 prevPos;
    public float speed;

    Transform crawlOnSurfaceObj;
    Transform surfaceObj;

    public bool debug = false;

    public float wobblyness = 10;
    public Transform body;
    Transform bodyParent;
    Transform bodyLookTarget;

    // Use this for initialization
    void Start () {
        prevPos = transform.position;

        if (lookDirObj == null)
        {
            lookDirObj = new GameObject().transform;
            lookDirObj.SetParent(transform);
        }

        // setup body parent for parenting the body to. this make the body wobble a bit.
        //if (body != null)
        //{
            bodyParent = new GameObject().transform;
            bodyParent.SetParent(transform.parent,false);
            bodyParent.Rotate(-90, 180, 0);

            bodyLookTarget = new GameObject().transform;
            bodyLookTarget.SetParent(transform, false);
            bodyLookTarget.Translate(0,5,0);

            body.SetParent(bodyParent);
        //}

        // count all children that are legs
        legCount = 0;
		for (int i = 0; i < transform.childCount; i++) {
			Transform leg = transform.GetChild (i);
			if (leg.GetComponent<placeFoot> () != null) {
				legCount++;
			}
		}

		// put all children that are legs into list
		legs = new Transform[legCount];
		feet = new Transform[legCount];
		for (int i = 0; i < transform.childCount; i++) {
			Transform leg = transform.GetChild (i);
            if (leg.GetComponent<placeFoot>() != null)
            {
                legs[i] = leg;
                feet[i] = leg.GetComponent<placeFoot>().legEnd;

                // find center of feet
                center += feet[i].position / legCount;

                // pass this script to each leg
                leg.GetComponent<placeFoot>().averageFootPositionsScript = GetComponent<averageFootPositions>();

                leg.GetComponent<placeFoot>().neighbourFeet = new placeFoot[2];
                Transform closestleg = findClosesFoot(leg);
                //				leg.GetComponent<placeFoot> ().neighbourFoot = closestleg.GetComponent<placeFoot> ();
                // get the closest leg (for checking the 2 adjecent legs are not moving simutaniously)
                leg.GetComponent<placeFoot>().neighbourFeet[0] = closestleg.GetComponent<placeFoot>();
                leg.GetComponent<placeFoot>().neighbourFeet[1] = findClosesFoot(leg, closestleg).GetComponent<placeFoot>();

                // if a body object is passed, then parent the leg start transforms to the body
                if (body != null)
                {
                    leg.GetComponent<placeFoot>().legStart.SetParent(body);
                }
            }
		}
        

        // set x and z center from root (center of mass), but keep the average height of the feet.
        center.x = transform.position.x;
		center.z = transform.position.z;

		// store center in local space
		center = transform.InverseTransformPoint (center);
        

        // get crawlOnSurfaceObj from placeFoot. optimize later...
        if (legs[0].GetComponent<placeFoot>().crawlOnSurfaceObj != null)
        {
            crawlOnSurfaceObj = legs[0].GetComponent<placeFoot>().crawlOnSurfaceObj;
            surfaceObj = crawlOnSurfaceObj.GetComponent<crawlOnSurface>().target.transform;
        }
    }

	Transform findClosesFoot (Transform leg , Transform skipThisLeg = default(Transform)){
		Transform closest = leg;
		float neighbourDist = 10000000000000000f;

		for (int j = 0; j < transform.childCount; j++) {
			Transform otherLeg = transform.GetChild (j);
			if (otherLeg.GetComponent<placeFoot> () != null) {
				float dist = (leg.transform.position - otherLeg.transform.position).magnitude;
				if (dist < neighbourDist && leg != otherLeg && otherLeg != skipThisLeg) {
					neighbourDist = dist;
					// store the closest one.
					closest = otherLeg;
				}
			}
		}
		return closest;
	}

	// Update is called once per frame
	void Update () {
		updateAveragePos ();
        
        bodyParent.position = Vector3.Lerp(bodyParent.position, averagePosition,.2f);
        // use up and down motion to drive x of the target,  resulting in side ways rotaion movement
        bodyLookTarget.localPosition = new Vector3( Mathf.Lerp(bodyLookTarget.localPosition.x, balanceDeviation.z*wobblyness*2000* speed, .5f), bodyLookTarget.localPosition.y, bodyLookTarget.localPosition.z);
        bodyParent.rotation = Quaternion.LookRotation(bodyLookTarget.position - averagePosition, transform.forward);
    }

    public void updateAveragePos(){

		if (crawlOnSurfaceObj.localPosition != prevPos)
			walkDirection = crawlOnSurfaceObj.localPosition - prevPos;
        if(walkDirection != Vector3.zero)
    		lookDirObj.rotation = Quaternion.LookRotation (surfaceObj.TransformDirection(walkDirection), transform.up);

        averagePosition = averagePositions(feet);
        
        balanceDeviation = transform.TransformPoint(center) - averagePosition;

		// set the values for all the legs
		for (int i = 0; i < legCount; i++) {
			legs [i].GetComponent<placeFoot> ().balanceDeviation = balanceDeviation;
			// checks the z pos in the space of the direction of the movement
			float frontness = lookDirObj.InverseTransformPoint(legs[i].position).z ;
			legs [i].GetComponent<placeFoot> ().frontness = frontness;
            if(debug)
    			Debug.DrawLine (averagePosition, feet [i].position, Color.black);
		}

        speed = (crawlOnSurfaceObj.localPosition - prevPos).magnitude;
        prevPos = crawlOnSurfaceObj.localPosition;
	}

    Vector3 averagePositions(Transform[] positionList)
    {
        averagePosition = new Vector3(0, 0, 0);
        for (int i = 0; i < positionList.Length; i++)
        {
            averagePosition += positionList[i].position / legCount;
        }
        return averagePosition;
    }
}
