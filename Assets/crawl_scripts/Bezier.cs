﻿using UnityEngine;
using System.Collections.Generic;

public class Bezier
{
    public Vector3 firstPoint;
    public Vector3 secondPoint;

    public Vector3 handlerFirstPoint;
    public Vector3 handlerSecondPoint;


    public int pointsQuantity;

    Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 handlerP0, Vector3 handlerP1, Vector3 p1)
    {
        float u = 1.0f - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0; //first term
        p += 3f * uu * t * handlerP0; //second term
        p += 3f * u * tt * handlerP1; //third term
        p += ttt * p1; //fourth term

        return p;
    }

	public Vector3[] calculatePoints()
	{
		Vector3[] points = new Vector3[pointsQuantity+1];

        points[0] = firstPoint;
        for (int i = 1; i < pointsQuantity; i++)
        {
            points[i] = CalculateBezierPoint((1f / pointsQuantity) * i, firstPoint, handlerFirstPoint, handlerSecondPoint, secondPoint);
		}
		points[pointsQuantity] = secondPoint;

        return points;
    }

    public void biasHandles()
    {
        handlerFirstPoint = biasPoint(firstPoint, secondPoint, .33f);
        handlerSecondPoint = biasPoint(firstPoint, secondPoint, .66f);
    }

    Vector2 biasPoint(Vector2 point1, Vector2 point2, float bias)
    {
        return new Vector2(point1.x + (point2.x - point1.x) * bias, point1.y + (point2.y - point1.y) * bias);
    }
}
