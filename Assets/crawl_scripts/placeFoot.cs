﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placeFoot : MonoBehaviour {


    //	todo: write anticipation behaviour. could be based on applied force. or if its not a physics obect applied input of the player and speed
    //	todo: write settle behaviour. if speed = 0 then average the positions of each leg, First the one that is furthes from its rest pos.


    public Transform legStart;
    public Transform legEnd;
    public Transform knee0;
    public Transform knee1;
    public Transform bodyCenter;
    public float legLength;

    public float stepSize = .5f;
    public float stepHeight = .5f; // will be multiplied with leg length

    float stepSpeed = 0;
    public int stepDuration = 10; //alternative to step speed -> amount of frames a step has to be executed in.
    float stepProgress = 0;
    bool startStep = true;
    public bool isMoving;
    float averageRestPosDeviation = 0;
    bool goingToRestPos = false;

    int restingTime = 0;

    bezierShape bezier;

    public Transform knee;

    Transform footPosTarget;
    Vector3 footPosUpdate;
    Vector3 footPosUpdate_unprojected;// use for comparing the foot pos with the neutral footpos for setteling the feet(cant do it with the projected one because distance check causes a loop ej)
    Vector3 footNeutralPos_local;
    Vector3 footStepPos; // used to not directly recalculate the foot pos becouse it is move by its parent.
    public float distFromNeutral;

    public float frontness; // how much the leg is in front , set by the average foot pos script
    public Vector3 balanceDeviation;

    public averageFootPositions averageFootPositionsScript;

    public Transform crawlOnSurfaceObj;
    crawlOnSurface crawlOnSurfaceScript;
    Transform surfaceTarget;
    //	public placeFoot neighbourFoot;
    public placeFoot[] neighbourFeet;

    public bool debug = false;

    void Start() {
        if (crawlOnSurfaceObj != null)
        {
            crawlOnSurfaceScript = crawlOnSurfaceObj.GetComponent<crawlOnSurface>();
            surfaceTarget = crawlOnSurfaceScript.target.transform;
        }

        footPosTarget = new GameObject().transform; // make an object for target positions for feet so it can be parented to the rock or whatever
        footPosTarget.position = legEnd.position;
        footPosTarget.SetParent(surfaceTarget); // set the parent of these transforms so that they rotate with it during locked foot positions

        footNeutralPos_local = transform.InverseTransformPoint(biasPoint(legStart.position, legEnd.position, .5f));
        footPosUpdate = transform.TransformPoint(footNeutralPos_local);
        legLength = (legStart.position - legEnd.position).magnitude;

        legEnd.position = footPosUpdate;
        footStepPos = footPosUpdate;

        bezier = GetComponent<bezierShape>();

        //stepSize = stepSize * legLength;
        stepHeight = stepHeight * legLength;

        bezier.lineColor = Color.white;

        if (debug)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Destroy(sphere.GetComponent<Collider>());
            Destroy(sphere.GetComponent<Rigidbody>());
            sphere.transform.parent = legEnd;
            sphere.transform.position = legEnd.position;
            sphere.transform.localScale = new Vector3(.1f, .1f, .1f);
            Material m = sphere.GetComponent<Renderer>().material;
            m.EnableKeyword("_EmissionColor");
            m.SetColor("_EmissionColor", Color.red);
            m.SetColor("_Color", Color.red);

        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Gizmos.DrawSphere(legEnd.position, .03f);
        //Gizmos.DrawSphere(footPosUpdate_unprojected, .015f);
        //Gizmos.DrawSphere(transform.TransformPoint(footNeutralPos_local), .01f);

        Gizmos.DrawLine(legStart.position, legEnd.position);
    }
                // Update is called once per frame
    void Update() {


        bezier.lineColor = Color.white;

        //// updateAveragePos for averageFootPositionsScript
        //updateAveragePos();


        // if this AND neighbour foot is reaching stretch limit, then anticipate the step now
        //		averageNeigbourCheck (neighbourFeet[0]);
        // fix this: places the foot and sees in the next frame they are over stretched i think



        // store potential new foot position
        //footPosUpdate = biasPoint(footPos, transform.TransformPoint(footNeutralPos), 2f);
        if (!goingToRestPos)
            footPosUpdate = calculateFootPos(footPosTarget.position);

        // make a step if foot goes underneath the body
        underneathBody();

        // if stretch limit is reached, make a step
        if (!goingToRestPos)
            stretchStep(1f);

        //// settle feet to rest pose
        settleFeet();

        // if one of the neigbourfeet is moving dont move this one.
        waitForNeighbours(); // sets start step to false if needed

        // if stretch gets too far after waiting for the neigbours, then make a step anyway
        stretchStep(3.5f);

        // insert footPosUpdate just this frame into the new target pos (world space)
        if (startStep) {
            footPosTarget.position = footPosUpdate;
            isMoving = true;
            stepSpeed = .2f;

            footPosUpdate_unprojected = footPosUpdate;
            //raycast
            if (crawlOnSurfaceScript != null)
                footPosTarget.position = raycast(footPosTarget.position, legStart.TransformDirection(new Vector3(0, -1, 0)));
        }

        if (debug)
            Debug.DrawLine(legEnd.position, footPosTarget.position, Color.magenta);



        //float disttt = (legStart.position - footPos).magnitude; ;
        //if (disttt > legLength)
        //{
        //    Vector3 stretchDirVec = (footPos - legStart.position).normalized * legLength;
        //    footPos = legStart.position + stretchDirVec;
        //}

        if (isMoving)
        {
            // move the foot
            translateFoot();
        }
        else
        {
            // plant foot
            legEnd.position = footPosTarget.position; // keep foot planted
            footStepPos = legEnd.position; // SET pos to start next step from
            restingTime++;
        }

        //fix over streched
        clampLegLength(legEnd);


        // place the knee
        float dist = (legStart.position - legEnd.position).magnitude;
        if (dist > legLength)
            dist = legLength;
        knee.position = biasPoint(legStart.position, legEnd.position, .5f) + transform.up * (legLength-dist)*.5f;
        
        if(bezier.enabled)
            bezier.updateLines();

        startStep = false;
    }

    Vector3 calculateFootPos(Vector3 currentFootPos)
    {
        if (averageFootPositionsScript.speed == 0)
            return transform.TransformPoint(footNeutralPos_local);


        // TODO: add a multiply for new step pos with the speed. so the new position is taking in acount the position of the body when the foot will land
        // maight have to add this AFTER the clamp.
        Vector3 newPos = averageFootPositionsScript.lookDirObj.forward * stepSize* legLength + transform.TransformPoint(footNeutralPos_local);

        //CLAMP new pos
        float dist = (legStart.position - newPos).magnitude; ;
        if (dist > legLength)
        {
            Vector3 stretchDirVec = (newPos - legStart.position).normalized * legLength;
            newPos = legStart.position + stretchDirVec;
        }

        // draw potential new position
        if (debug)
            Debug.DrawLine(legEnd.position, newPos, Color.magenta);

        return newPos;
    }

    void updateAveragePos(){
        if (balanceDeviation.magnitude > legLength *.5f && frontness > 0) {
            //print(gameObject.name);
            //print(balanceDeviation.magnitude);
			// todo, use the foot with the highest frontness instead of moving all feet with frontness > 0
			startStep = true;
			bezier.lineColor = Color.yellow;

			// force update all feet so this frame not all feet with frontness > 0 are moved
			averageFootPositionsScript.updateAveragePos();
		}
    }

    void settleFeet() { 
        // settle feet to rest pose
        if (restingTime > 20)
        {
            //if (averageRestPosDeviation > stepSize * .2f)
            distFromNeutral = (footPosUpdate_unprojected - transform.TransformPoint(footNeutralPos_local)).magnitude;
            if (distFromNeutral > stepSize* legLength * 2f)
            {
                print("restPose"+ restingTime);
                startStep = true;
                goingToRestPos = true;
                footPosUpdate = transform.TransformPoint(footNeutralPos_local);
                bezier.lineColor = Color.cyan;
            }
        }
    }

    void waitForNeighbours()
    {
        if (neighbourFeet.Length == 0) {
            print("no neightbours found");
        }
        // if one of the neigbourfeet is moving dont move this one.
        for (int i = 0; i < neighbourFeet.Length; i++)
        {
            if (startStep && neighbourFeet[i].isMoving)
            {
                startStep = false;
                bezier.lineColor = Color.red;
            }
        }
    }
    void underneathBody()
    {
        // make a step if foot goes underneath the body
        if (transform.InverseTransformPoint(footPosTarget.position).z > 0)
        {
            startStep = true;
            footPosUpdate = transform.TransformPoint(footNeutralPos_local);
			bezier.lineColor = Color.blue;
        }
    }
    void stretchStep(float treshold)
    {
        // teshold is how far back the foot must go before it steps
        // if stretch limit is reached, make a step
        //restPosDeviation = (transform.TransformPoint(footNeutralPos) - legEnd.position).magnitude;
        //if (restPosDeviation > stepSize)

        //Debug.DrawLine(legEnd.position, transform.TransformPoint(footNeutralPos));
        // check how much the foot is behind the neutral pos. in the space of the lookDirObj.
        averageRestPosDeviation = averageFootPositionsScript.lookDirObj.InverseTransformVector(footPosUpdate_unprojected - transform.TransformPoint(footNeutralPos_local)).z;

        float dist = (legStart.position - legEnd.position).magnitude;
        if (dist > legLength*stepSize * treshold && averageRestPosDeviation < 0)
        {
            startStep = true;
            bezier.lineColor = Color.green;
        }
    }
    void clampLegLength(Transform placeAtClampedPos)
    {
        float dist = (legStart.position - placeAtClampedPos.position).magnitude; ;
        if (dist > legLength)
        {
            Vector3 stretchDirVec = (placeAtClampedPos.position- legStart.position).normalized*legLength;
            placeAtClampedPos.position = legStart.position + stretchDirVec;
        }
    }
	void translateFoot()
    {
        Vector3 stepVecNormalised = (footPosTarget.position - footStepPos).normalized;
        //Vector3 stepVec = (footPosTarg - footStepPos).normalized * stepSize * stepSpeed;
        float dist = (footPosTarget.position - footStepPos).magnitude;
        
        float targetBias = stepProgress / stepDuration;
        Vector3 stepVec = stepVecNormalised * dist * targetBias;
        if (debug)
            Debug.DrawLine(legEnd.position, footPosTarget.position, Color.yellow);

        if (dist > stepVec.magnitude) {
            float stepHeightMult = stepHeight * (targetBias * (1 - targetBias));

            footStepPos = footStepPos + stepVec + legEnd.up * 3f* stepHeightMult;
            legEnd.position = footStepPos;

            stepProgress++;
        } else {
            // foot landed, last frame of translateFoot() before next step starts
            footStepPos = footPosTarget.position;
            legEnd.position = footPosTarget.position; // keep foot planted

            isMoving = false;
            stepSpeed = 0;
            stepProgress = 0;
            restingTime = 0;
            goingToRestPos = false;

        }
	}

    void averageNeigbourCheck(placeFoot neighbourFoot)
    {

        if (averageRestPosDeviation > stepSize * .5f && neighbourFoot.averageRestPosDeviation > neighbourFoot.stepSize * .5f && !startStep)
        {
            if (frontness > neighbourFoot.frontness)
            {
                startStep = true;
                bezier.lineColor = Color.cyan;
            }
        } // places the foot and sees in the next frame they are over stretched i think

    }
    Vector3 biasPoint(Vector3 point1, Vector3 point2, float bias)
    {
        return new Vector3(point1.x + (point2.x - point1.x) * bias
            , point1.y + (point2.y - point1.y) * bias
            , point1.z + (point2.z - point1.z) * bias);
    }



    Vector3 raycast(Vector3 fromPos, Vector3 dir)
    {
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

        //int layerMask = 9;

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(fromPos + transform.TransformDirection(Vector3.up), dir, out hit, Mathf.Infinity, layerMask))
        {
            //Debug.Log("Did Hit");

            float dist = (legStart.position - hit.point).magnitude; ;
            if (dist < legLength)// if not over stretched use this point
                return hit.point;
        }

        Vector3 fromPosLocal = surfaceTarget.InverseTransformPoint(fromPos);
        Vector3 closestPoint = crawlOnSurfaceScript.NearestPointOnMesh(fromPosLocal);
        if (debug)
            Debug.DrawLine(fromPos, closestPoint, Color.red);
        bezier.lineColor = Color.magenta;
        return closestPoint;
        //return fromPos;
    }

}
