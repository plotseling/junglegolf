﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crawlBehaviour : MonoBehaviour {

    public int breakPercent = 30;
    public Vector2 breakTimeRange = new Vector2(10,30);
    int breakTime = 0;
    public Vector2 walkTimeRange = new Vector2(10, 30);
    int walkTime= 0;


    crawlOnSurface crawlOnSurfaceScript;
    float defaultCrawlSpeed;
    float defaultDeviationStrength;

    // Use this for initialization
    void Start () {
        crawlOnSurfaceScript = GetComponent<crawlOnSurface>();

        defaultCrawlSpeed = crawlOnSurfaceScript.crawlSpeed;
        defaultDeviationStrength = crawlOnSurfaceScript.deviationStrength;
    }
	
	// Update is called once per frame
	void Update () {

        // BREAKING
		if (Random.value*100 < breakPercent && breakTime == 0)
            breakTime = Random.Range((int)breakTimeRange[0], (int)breakTimeRange[1]);
        if (breakTime != 0)
        {
            breakTime--;
            crawlOnSurfaceScript.crawlSpeed = 0;
            crawlOnSurfaceScript.deviationStrength = 0;
        }
        else
        {
            crawlOnSurfaceScript.crawlSpeed = defaultCrawlSpeed;
            crawlOnSurfaceScript.deviationStrength = defaultDeviationStrength;
        }


	}
}
