﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateControl : MonoBehaviour {

	float mouseSpeed = 5f;
    float xSpeed = 0;
    float ySpeed = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		float mouseSpeed_x = Input.GetAxis ("Mouse X");
		float mouseSpeed_y = Input.GetAxis ("Mouse Y");
        if (Input.GetAxis("Mouse X") != 0)
			transform.Rotate(new Vector3(0,-mouseSpeed*mouseSpeed_x,0),Space.World);
		if(Input.GetAxis("Mouse Y") != 0)
			transform.Rotate(new Vector3(mouseSpeed*mouseSpeed_y,0,0),Space.World);

        //if (Input.touchCount > 1 && Input.GetTouch(0).phase == TouchPhase.)
        //{
        //    ySpeed = 0;
        //    xSpeed = 0;
        //}
        if (Input.touchCount > 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            ySpeed = Input.GetTouch(0).deltaPosition.y * .5f;
            xSpeed = Input.GetTouch(0).deltaPosition.x * -.5f;
        }

        transform.Rotate(new Vector3(ySpeed, xSpeed, 0), Space.World);
        xSpeed *= .99f;
        ySpeed *= .99f;
    }
}
