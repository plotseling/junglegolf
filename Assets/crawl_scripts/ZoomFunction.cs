﻿using UnityEngine;
using System.Collections;

public class ZoomFunction : MonoBehaviour {

	public float zoomSpeed = 10;
	float fov = 60;
	Camera cam;


    void Start() {
		cam = gameObject.GetComponent<Camera> ();
		fov = cam.fieldOfView;
    }

	void Update () {

		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		if (scroll != 0.0f) {
			fov -= scroll * zoomSpeed;
        }

        
		cam.fieldOfView = Mathf.Lerp(cam.fieldOfView,fov,.1f);

       // Camera.main.orthographicSize = Mathf.MoveTowards (Camera.main.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
    }
}