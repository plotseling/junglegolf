﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cam_targets : MonoBehaviour
{

    float mouseSpeed = 2f;

    public Transform folowObj;
    Transform camPos;

    public Transform horizontalRot;
    Transform verticalRot;

    Transform horizontalLerped;
    Transform verticalLerped;
    Transform posLerped;

    float camDist;

    public bool firstPerson = true;

    public bool keepCamHeightPos = true;
    float camHeightPos = 0;

    // Use this for initialization
    void Start()
    {

        //horizontalRot = new GameObject(name = "horizontalRot").transform;
        verticalRot = new GameObject(name = "verticalRot").transform;
        verticalRot.SetParent(horizontalRot);
        horizontalRot.position = folowObj.position;

        camPos = new GameObject(name = "camPos").transform;
        camPos.SetParent(verticalRot);
        camPos.position = transform.position;

        camHeightPos = camPos.localPosition.y;
        print(camHeightPos);

        horizontalLerped = Instantiate(horizontalRot.gameObject, horizontalRot.parent).transform;
        verticalLerped = horizontalLerped.GetChild(0);
        posLerped = verticalLerped.GetChild(0);
        camDist = (posLerped.position - verticalLerped.position).magnitude;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        horizontalRot.position = folowObj.position;
        horizontalLerped.position = folowObj.position;

        float mouseSpeed_x = Input.GetAxis("Mouse X");
        float mouseSpeed_y = Input.GetAxis("Mouse Y");
        if (Input.GetAxis("Mouse X") < 0)
        {
            horizontalRot.Rotate(new Vector3(0, 2 * mouseSpeed * mouseSpeed_x, 0));
        }
        if (Input.GetAxis("Mouse X") > 0)
        {
            horizontalRot.Rotate(new Vector3(0, 2 * mouseSpeed * mouseSpeed_x, 0));
        }

        
        // make a limit to mouse input or someting.... now its so big cam flips over...
        //		or make cam faster or another way so the angle doesnt become so big the angle lerp would flip
        if (Input.GetAxis("Mouse Y") < 0)
        {
            verticalRot.Rotate(new Vector3(-2 * mouseSpeed * mouseSpeed_y, 0, 0));
            // weird fix because negative rot becomes positve ...
            //			print(verticalRot.localEulerAngles.x);
            if (verticalRot.localEulerAngles.x > 70 && verticalRot.localEulerAngles.x < 90)
            {
                verticalRot.localRotation = Quaternion.Euler(new Vector3(70, 0, 0));
            }
        }
        if (Input.GetAxis("Mouse Y") > 0)
        {
            // weird fix because negative rot becomes positve ...
            if (verticalRot.localEulerAngles.x > 360 - 80 || verticalRot.localEulerAngles.x < 80)
            {
                verticalRot.Rotate(new Vector3(-2 * mouseSpeed * mouseSpeed_y, 0, 0));
            }
        }
        //		or make cam faster or another way so the angle doesnt become so big the angle lerp would flip

        //		if (Vector3.Angle (verticalLerped.forward, verticalRot.forward) > 120) {
        //			print (Vector3.Angle( verticalLerped.forward,verticalRot.forward));
        //		}
        verticalLerped.rotation = Quaternion.Lerp(verticalLerped.rotation, verticalRot.rotation, .1f);

        transform.position = Vector3.Lerp(transform.position, posLerped.position, .05f);

        transform.LookAt(verticalRot, Vector3.up);

    }
}
