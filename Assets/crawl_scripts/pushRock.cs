﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pushRock : MonoBehaviour {

    public Transform horizontalRot;
    Rigidbody rb;

    // Use this for initialization
    void Start () {

        rb = gameObject.GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.anyKey)
        {
            rb.AddForce(horizontalRot.forward*1000);
        }
    }
}
