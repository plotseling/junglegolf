﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Lighting/BasicLightingPerFragment"
{
    Properties
    {
		_MainTex("Main Texture", 2D) = "white" {}
		_Ramp("ramp", 2D) = "white" {}
		_Normal ("ramp", 2D) = "white" {}
 

		[Header(iridescence)]
		_Spectrum("spectrum", Range(0., 1.)) = 0.2
		_DifIridescentMult("spectrum Backlit", Range(0., 1.)) = 1.
		_AmbientSpec("specualr cutoff", Range(-1., 1.)) = 0.1

        [Header(Ambient)]
        _AmbColor ("Color", color) = (1., 1., 1., 1.)
 
        [Header(Diffuse)]
        _DifColor ("Color", color) = (1., 1., 1., 1.)
 
        [Header(Specular)]
        _Shininess ("Shininess", Range(0.1, 10)) = 1.
        _SpecColor ("Specular color", color) = (1., 1., 1., 1.)
 
    }
 
    SubShader
    {
        Pass
        {
            Tags { "RenderType"="Opaque" "Queue"="Geometry" "LightMode"="ForwardBase" }


            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
 
            // Change "shader_feature" with "pragma_compile" if you want set this keyword from c# code
            #pragma shader_feature __ _SPEC_ON
 
            #include "UnityCG.cginc"
			//#include "UnityLightingCommon.cginc"
			#include "Lighting.cginc"

			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			// shadow helper functions and macros
			#include "AutoLight.cginc"

            struct v2f {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
				SHADOW_COORDS(1) // put shadows data into TEXCOORD1
                float3 worldPos : TEXCOORD2;
                float3 worldNormal : TEXCOORD3;
				half3 worldRefl : TEXCOORD4;

				fixed3 diff : COLOR0;
				fixed3 ambient : COLOR1;

            };
 
            v2f vert(appdata_base v, float4 vertex : POSITION, float3 normal : NORMAL)
            {
                v2f o;
                // World position
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
 
                // Clip position
                o.pos = mul(UNITY_MATRIX_VP, float4(o.worldPos, 1.));
 
                // Normal in WorldSpace
                o.worldNormal = normalize(mul(v.normal, (float3x3)unity_WorldToObject));
 
                o.uv = v.texcoord;


				// reflection stuff
				// compute world space position of the vertex
				float3 worldPos = mul(unity_ObjectToWorld, vertex).xyz;
				// compute world space view direction
				float3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
				// world space normal
				float3 worldNormal = UnityObjectToWorldNormal(normal);
				// world space reflection vector
				o.worldRefl = reflect(-worldViewDir, worldNormal);


				// shadow casting
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				o.diff = nl * _LightColor0.rgb;
				o.ambient = ShadeSH9(half4(worldNormal, 1));
				// compute shadows data
				TRANSFER_SHADOW(o)


                return o;
            }

			sampler2D _MainTex;
			sampler2D _Ramp;
			sampler2D _Normal;
			
			fixed _Spectrum;
			fixed _AmbientSpec;
            //fixed4 _LightColor0;
           
			fixed4 _DifColor;
			fixed _DifIridescentMult;
			
            fixed _Shininess;
            //fixed4 _SpecColor;

            fixed4 _AmbColor;
 
            sampler2D _EmissionTex;
            fixed4 _EmiColor;
            fixed _EmiVal;
 
            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 c = tex2D(_MainTex, i.uv);
 
                // Light direction
                float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
 
                // Camera direction
                float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
 
                float3 worldNormal = normalize(i.worldNormal);
 
				fixed shadow = SHADOW_ATTENUATION(i);

                // Compute the diffuse lighting
                fixed3 NdotL = max(0., dot(worldNormal, lightDir) * _LightColor0);
                fixed3 dif = NdotL * _LightColor0 * _DifColor *shadow;

                fixed3 light = dif + _AmbColor;
				//light = i.diff * shadow + i.ambient;
 
                // Compute the specular lighting
                float3 refl = normalize(reflect(-lightDir, worldNormal));
                fixed3 spec = dot(refl, viewDir) * _LightColor0 * _SpecColor*(shadow);
 
				fixed3 iridescence = dif + max(spec, _AmbientSpec);
				//light = spec;
				
				fixed3 ramp = tex2D(_Ramp, float2(iridescence.r*_Spectrum,0) )*2;

				c.rgb *= ramp * (light*(1 - _DifIridescentMult) + _DifIridescentMult) + pow(max(0.1, spec), _Shininess);
				//c.rgb = spec;

                half3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
                half3 worldRefl = reflect(-worldViewDir, worldNormal);
                half4 skyData = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, worldRefl);
                half3 skyColor = DecodeHDR (skyData, unity_SpecCube0_HDR);
				// output it!
				c.rgb *= skyColor;


				//fixed4 col = tex2D(_MainTex, i.uv);
				//// darken light's illumination with shadow, keep ambient intact
				//fixed3 lighting = i.diff * shadow + i.ambient;
				//col.rgb *= lighting;

                return c;
            }
 
            ENDCG
        }
		// shadow casting support
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}
