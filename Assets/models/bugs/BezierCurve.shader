﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/BezierCurve"
{
	Properties
	{
		_MainTex("Texture", 2D) = "grey" {}
		p0("Control point", vector) = (0, 0, 0)
		p1("Control point", vector) = (1, 1, 1)
		p2("Control point", vector) = (0, 1, 1)
		p3("Control point", vector) = (1, 0, 0)
	}
		SubShader
	{
		Tags{ "RenderType" = "Opaque" }

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

		struct v2f
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	sampler2D _MainTex;
	float4 _MainTex_ST;
	float3 p0;
	float3 p1;
	float3 p2;
	float3 p3;

	v2f vert(appdata_base v)
	{
		v2f o;
		float3 begin = float3(-0.5, v.vertex.y, v.vertex.z);
		float3 end = float3(0.5, v.vertex.y, v.vertex.z);

		float t = v.vertex.x;

		float u = 1.0f - t;
		float tt = t * t;
		float uu = u * u;
		float uuu = uu * u;
		float ttt = tt * t;

		float3 p = uuu * p0; //first term
		p += 3 * uu * t * p1; //second term
		p += 3 * u * tt * p2; //third term
		p += ttt * p3; //fourth term

		//return p;

		v.vertex.xyz += p;


		o.pos = UnityObjectToClipPos(v.vertex);

		//o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		return tex2D(_MainTex, i.uv);
	}
		ENDCG
	}
	}
}
