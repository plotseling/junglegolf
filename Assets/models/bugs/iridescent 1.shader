﻿Shader "Lit/Diffuse With Shadows"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_Ramp("ramp", 2D) = "white" {}
		_Normal("ramp", 2D) = "white" {}


		[Header(iridescence)]
		_Spectrum("spectrum", Range(0., 1.)) = 0.2
		_DifIridescentMult("spectrum Backlit", Range(0., 1.)) = 1.
		_AmbientSpec("specualr cutoff", Range(-1., 1.)) = 0.1

		[Header(Ambient)]
		_AmbColor("Color", color) = (1., 1., 1., 1.)

		[Header(Diffuse)]
		_DifColor("Color", color) = (1., 1., 1., 1.)

		[Header(Specular)]
		_Shininess("Shininess", Range(0.1, 10)) = 1.
		_SpecColor("Specular color", color) = (1., 1., 1., 1.)

	}
	SubShader
	{
		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
					// shadow helper functions and macros
			#include "AutoLight.cginc"

				struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				SHADOW_COORDS(1) // put shadows data into TEXCOORD1
					float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				half3 worldRefl : TEXCOORD4;

				fixed3 diff : COLOR0;
				fixed3 ambient : COLOR1;

			};

			v2f vert(appdata_base v, float4 vertex : POSITION, float3 normal : NORMAL)
			{
				v2f o;
				// World position
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);

				// Clip position
				o.pos = mul(UNITY_MATRIX_VP, float4(o.worldPos, 1.));

				// Normal in WorldSpace
				o.worldNormal = normalize(mul(v.normal, (float3x3)unity_WorldToObject));

				o.uv = v.texcoord;


				// reflection stuff
				// compute world space position of the vertex
				float3 worldPos = mul(unity_ObjectToWorld, vertex).xyz;
				// compute world space view direction
				float3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
				// world space normal
				float3 worldNormal = UnityObjectToWorldNormal(normal);
				// world space reflection vector
				o.worldRefl = reflect(-worldViewDir, worldNormal);


				// shadow casting
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				o.diff = nl * _LightColor0.rgb;
				o.ambient = ShadeSH9(half4(worldNormal, 1));
				// compute shadows data
				TRANSFER_SHADOW(o)


					return o;
			}

			sampler2D _MainTex;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
				fixed shadow = SHADOW_ATTENUATION(i);
				// darken light's illumination with shadow, keep ambient intact
				fixed3 lighting = i.diff * shadow + i.ambient;
				col.rgb *= lighting;
				return col;
			}
			ENDCG
		}

		// shadow casting support
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}
