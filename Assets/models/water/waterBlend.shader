﻿Shader "Custom/waterBlend"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_NormalMap("Normalmap", 2D) = "bump" {}
		_HeightMap("Height", 2D) = "bump" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _HeightMap;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_NormalMap;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			//o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap + float2(-_Time.x, 0)));
			
			fixed4 h1 = tex2D(_HeightMap, IN.uv_NormalMap + float2(-_Time.x, 0));
			fixed4 h2 = tex2D(_HeightMap, IN.uv_NormalMap + float2(-_Time.x*2, 0));
			if (h1.r < h2.r) {
				//c.rgb = float3(1, 0, 0);
				o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap + float2(-_Time.x*2, 0)));
			}

            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
