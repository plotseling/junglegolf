﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballMove : MonoBehaviour
{

    Vector3 prevPos;
    Vector3 difference;

    public Transform rootTransform;

    bool m_HitDetect;
    RaycastHit m_Hit;

    int hitTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        prevPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        hitTime++;
        if (hitTime > 50)
        {
            difference = transform.position - prevPos;
            prevPos = transform.position;

            float length = difference.magnitude;
            m_HitDetect = Physics.Raycast(transform.position, -difference, out m_Hit, difference.magnitude);
            //m_HitDetect = Physics.BoxCast(transform.position - difference*.5f, new Vector3(.5f * length, .05f, .05f), -difference, out m_Hit, transform.rotation, difference.magnitude);

            if (m_HitDetect)
            {
                Debug.DrawRay(transform.position, -difference, Color.red);
                if (m_Hit.collider.gameObject.layer == 9)
                {
                    m_Hit.collider.transform.Translate(difference);
                    m_Hit.collider.attachedRigidbody.velocity = difference * 100;
                    hitTime = 0;
                }
            }
            else
            {
                Debug.DrawRay(transform.position, -difference, Color.green);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.layer == 9)
        //{
        //    float hitpower = difference.magnitude;
        //    float hitDirection = rootTransform.InverseTransformVector(difference).x;
        //    float ball_relative_xPos = Mathf.Abs(rootTransform.InverseTransformPoint(other.transform.position).x);// xpos relative to stick
        //    //float ballSideOfStick = ball_relative_xPos / Mathf.Abs(ball_relative_xPos);
        //    ball_relative_xPos += hitDirection * (.06f + hitpower * .5f);// .06f is ball radius and club thickness


        //    other.transform.Translate(new Vector3(ball_relative_xPos, 0, 0), rootTransform);
        //    other.attachedRigidbody.velocity = rootTransform.TransformVector(new Vector3(hitpower * hitDirection * 100, 0, 0));
        //}
    }
}