﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveSoftly : MonoBehaviour
{
    public float Speed = 1;
    public float Amplitude = 1;

    Vector3 origPos;
    // Start is called before the first frame update
    void Start()
    {
        origPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(Amplitude * .4f * Mathf.Cos(Time.time* Speed), origPos.y, Amplitude* .2f * Mathf.Cos(Time.time*1.5213f* Speed));
    }
}
